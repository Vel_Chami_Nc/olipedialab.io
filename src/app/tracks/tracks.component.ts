import { Component, OnInit } from '@angular/core';
import {Track}               from '../track';
import {TRACKS}              from '../mock-tracks';
import {TrackService}        from '../track.service'
@Component({
  selector: 'app-tracks',
  templateUrl: './tracks.component.html',
  styleUrls: ['./tracks.component.scss']
})
export class TracksComponent implements OnInit {

  tracks: Track[];
  pub_date = new Date();
  p: number = 1;

  constructor( private trackService: TrackService) { }

  getTracks(){
    return this.trackService.getTracks()
      .subscribe(tracks => this.tracks = tracks)
  }

  ngOnInit() {
     this.getTracks();
  }

}
