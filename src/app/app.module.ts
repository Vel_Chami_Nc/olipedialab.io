import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import {NgxPaginationModule} from 'ngx-pagination';
import { AppComponent } from './app.component';
import { TracksComponent } from './tracks/tracks.component';
import { TrackService } from './track.service';
import { TrackDetailComponent } from './track-detail/track-detail.component';
import { FooterComponent } from './footer/footer.component';



@NgModule({
  declarations: [
    AppComponent,
    TracksComponent,
    TrackDetailComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxPaginationModule
  ],
  providers: [TrackService],
  bootstrap: [AppComponent]
})
export class AppModule {

 }
