import { Injectable } from '@angular/core';
import {Track}        from './track';
import {TRACKS}       from './mock-tracks';
import { Observable } from 'rxjs/Observable';
import { of }         from 'rxjs/observable/of';
@Injectable()
export class TrackService {

getTracks(): Observable<Track[]>{
  return of(TRACKS);
}

getTrack(id: number): Observable<Track>{
  return of(TRACKS.find(track => track.id === id));
}
  constructor() { }

}
