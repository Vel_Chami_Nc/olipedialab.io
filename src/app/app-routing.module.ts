import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TrackDetailComponent} from './track-detail/track-detail.component';
import {TracksComponent} from './tracks/tracks.component';
const routes: Routes = [
  { path: '', redirectTo: '/tracks', pathMatch: 'full' },
  { path: 'track/:id', component: TrackDetailComponent },
  { path: 'tracks',     component: TracksComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
