import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import {Track} from '../track';
import {TrackService} from '../track.service';
@Component({
  selector: 'app-track-detail',
  templateUrl: './track-detail.component.html',
  styleUrls: ['./track-detail.component.scss']
})
export class TrackDetailComponent implements OnInit {

  @Input() track: Track;
  pub_date = new Date();
  constructor(
    private route: ActivatedRoute,
    private trackService: TrackService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getTrack();
  }
  getTrack(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.trackService.getTrack(id)
      .subscribe(track => this.track = track);
  }
  goBack(): void{
    this.location.back();
  }
}
