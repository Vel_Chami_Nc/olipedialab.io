import { Component } from '@angular/core';
import * as Typed from 'typed.js';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'OliPedia';

  ngOnInit() {
    let options = {
         strings: ["தமிழ் விக்கிபீடியா கட்டுரைகள். ", "பொதுவுடமை புத்தகங்கள்."],
         typeSpeed: 100,
         backSpeed: 100,
         showCursor: true,
         cursorChar: "|"
       }

     let typed = new Typed(".typing-element", options);
  }
}
