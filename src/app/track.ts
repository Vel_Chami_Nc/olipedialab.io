export class Track {
  id: number;
  name: string;
  pub_date: string;
  url: string;
  image: string;
  topic: any;
}
