import {Track} from './track';
export const TRACKS: Track[] = [
  {
     id: 1,
     name: 'தமிழ்',
     url: "https://ia801504.us.archive.org/0/items/tamil_201711/tamil.mp3",
     image:  "https://ia801503.us.archive.org/22/items/ThaiPongal/pongal.jpg?cnt=0",
     topic: ['தமிழ்','வரலாறு'],
     pub_date: "2017-11-16",
   },
   {
      id: 2,
      name: 'இந்திரா காந்தி',
      url: "https://ia801505.us.archive.org/6/items/indiragandhi/indiragandhi.mp3",
      image:  "https://ia601505.us.archive.org/6/items/indiragandhi/indiragandhi.jpg?cnt=0",
      topic: ['தமிழ்','சுயசரிதை'],
      pub_date: "2017-12-25",
    },
    {
       id: 3,
       name: 'மாணிக்கனார்',
       url: "https://ia801508.us.archive.org/27/items/manickanar_201712/manickanar.mp3",
       image:  "https://ia800101.us.archive.org/6/items/manickanar_201712/manickanar.jpg?cnt=0",
       topic: ['தமிழ்','சுயசரிதை'],
       pub_date: "2017-12-26"
     },
     {
        id: 4,
        name: 'அன்னை தெரேசா',
        url: "https://ia801506.us.archive.org/10/items/teresa_201712/teresa.mp3",
        image:  "https://ia800102.us.archive.org/1/items/teresa_201712/teresa.jpg?cnt=0",
        topic: ['சுயசரிதை'],
        pub_date:  "2017-12-23"
      },
      {
         id: 5,
         name: 'தந்தை பெரியார்',
         url: "https://ia801509.us.archive.org/7/items/periyar_201712/periyar.mp3",
         image:  "https://ia800101.us.archive.org/18/items/periyar_201712/periyar.jpg?cnt=0",
         topic: ['தமிழ்நாடு','சுயசரிதை'],
         pub_date: "2017-12-23"
       },
       {
          id: 6,
          name:'காமராசர்',
          url: "https://ia801503.us.archive.org/0/items/k_kamaraj/k_kamaraj.mp3",
          image:  "https://ia800104.us.archive.org/8/items/k_kamaraj/kamaraj.jpg?cnt=0",
          topic: ['தமிழ்நாடு','சுயசரிதை','தமிழ்','காங்கிரஸ்','முதலமைச்சர்'],
          pub_date: "2017-12-23"
        },
        {
           id: 7,
           name: 'சுபாஷ் சந்திர போஸ்',
           url: "https://ia801501.us.archive.org/0/items/subhash_chandra_bose/subhash_chandra_bose.mp3",
           image:  "https://ia800600.us.archive.org/8/items/subhash_chandra_bose/nethaji.jpg?cnt=0",
           topic: ['சுயசரிதை','தமிழ்'],
           pub_date: "2017-12-23"
         },
         {
            id: 8,
            name: 'சீனிவாச இராமானுசன்',
            url: "https://ia801508.us.archive.org/27/items/srinivasa_ramanujan/srinivasa_ramanujan.mp3",
            image:  "https://ia800103.us.archive.org/21/items/srinivasa_ramanujan/1.jpg?cnt=0",
            topic: ['சுயசரிதை','தமிழ்'],
            pub_date: "2017-12-23"
          },
          {
             id: 9,
             name: 'ஆ. ப. ஜெ. அப்துல் கலாம்',
             url: "https://ia800100.us.archive.org/13/items/abdulkalam_201711/abdulkalam.mp3",
             image:  "https://ia800600.us.archive.org/8/items/subhash_chandra_bose/nethaji.jpg?cnt=0",
             topic: ['சுயசரிதை','தமிழ்'],
             pub_date: "2017-11-22"
           },
           {
              id: 10,
              name: 'தமிழ்நாடு',
              url: "https://ia801506.us.archive.org/13/items/tamilnadu_201711/tamilnadu.mp3",
              image:  "https://archive.org/download/indiragandhi/indiragandhi.jpg",
              topic: ['சுயசரிதை','தமிழ்'],
              pub_date: "2017-11-18"
            },
            {
               id: 11,
               name: 'பாரதியார்',
               url: "https://ia801507.us.archive.org/23/items/bharathi_201712/bharathi.mp3",
               image:  "https://ia801507.us.archive.org/23/items/bharathi_201712/bharathi.jpg?cnt=0",
               topic: ["பாரதியார்", "பாரதி", "கவிஞர்", "தமிழ்"],
               pub_date: " 2017-12-29 "
             },
             {
                id: 12,
                name: 'அம்பேத்கர்',
                url: "https://ia801502.us.archive.org/24/items/ambedkar_201712/ambedkar.mp3",
                image:  "https://ia601502.us.archive.org/24/items/ambedkar_201712/ambedkar.jpg?cnt=0",
                topic: [ "அம்பேத்கார்", "சுயசரிதை"],
                pub_date: " 2017-12-31 "
              },
              {
                 id: 13,
                 name: 'ஜவகர்லால் நேரு',
                 url: "https://ia801503.us.archive.org/6/items/nehru/nehru.mp3",
                 image:  "https://ia801503.us.archive.org/6/items/nehru/nehru.jpg?cnt=0",
                 topic: [  "நேரு", "சுயசரிதை", "ஜவகர்லால் "],
                 pub_date: "2018-01-02"
               },
               {
                  id: 14,
                  name: 'மகாத்மா காந்தி',
                  url: "https://ia801503.us.archive.org/6/items/nehru/nehru.mp3",
                  image:  "https://ia801506.us.archive.org/26/items/gandhi_201801/gandhi.jpg?cnt=0",
                  topic: [  "காந்தி", "சுயசரிதை", "இந்தியா"],
                  pub_date: "2018-01-02"
                },
               {
                  id: 15,
                  name: 'மருது சகோதரர்கள்',
                  url: "https://ia801503.us.archive.org/6/items/nehru/nehru.mp3",
                  image:  "https://ia801501.us.archive.org/7/items/MaruthuSagotharargal/maruthu.jpg?cnt=0",
                  topic: [  "மருது", "சுயசரிதை"],
                  pub_date: "2018-01-02"
                },
                {
                  id: 16,
                  name: 'திப்பு சுல்தான்',
                  url: "https://ia801503.us.archive.org/6/items/nehru/nehru.mp3",
                  image:  "https://ia601500.us.archive.org/31/items/olipedia_Tipu/tipu.jpg?cnt=0",
                  topic: [  "திப்பு", "சுயசரிதை"],
                  pub_date: "2018-01-02"
                },
                 {
                  id: 17,
                  name: 'சுவாமி விவேகானந்தர்',
                  url: "https://ia801503.us.archive.org/6/items/nehru/nehru.mp3",
                  image:  "https://ia801507.us.archive.org/19/items/vivekananda_201801/vivekananda.jpg?cnt=0",
                  topic: [  "சுவாமி", "சுயசரிதை","விவேகானந்தர்"],
                  pub_date: "2018-01-02"
                },
                {
                  id: 17,
                  name: 'வேலு நாச்சியார்',
                  url: "https://ia801503.us.archive.org/6/items/nehru/nehru.mp3",
                  image:  "https://ia601505.us.archive.org/26/items/velunachiyar/velunachiyar.jpg?cnt=0",
                  topic: [  "வேலு", "சுயசரிதை","நாச்சியார்"],
                  pub_date: "2018-01-02"
                },
];
